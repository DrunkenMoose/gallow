(function() {
  var app = angular.module('gallow', []);  

  app.controller('recordsController', function(){
    this.mistakes = 0;
    this.allRecords = records;
    this.alphavit = "абвгдеёжзийклмнопрстуфхцчшщъыьэюя";
    this.result = "";   
    this.clickedLetters = " ";     
    this.gameFlag = true; //Можно ли продолжать играть
    this.firstCall = true; //Вызов функции при перезагрузки сайта

    //Получение случайной записи
    this.getRandomRecord = function(){
      var n = records.length;
      var num = parseInt(Math.random() * n, 10);
      this.currentRecord = records[num];    
      this.currentQuote = this.currentRecord.quote.toLowerCase();
      this.clickedLetters = " ";
      this.result = this.baseShowQuote(); //поле с отгаданными буквами
      this.mistakes = 0;      
      this.gameFlag = true;
      this.firstCall = false;
    };

    //отображение начального поля тирешек
    this.baseShowQuote = function(){
      var answer = "";      
      for (var i = 0; i < this.currentQuote.length; i++) {
        if(this.alphavit.indexOf(this.currentQuote[i]) >= 0){
          answer += "-";
        }else{
          answer += this.currentQuote[i];
        }
      }
      return answer;
    };

    //обработка нажатой кнопки
    this.checkLetter = function(letter){            
      if(!this.isClicked(letter)){
        this.clickedLetters += letter;       
      }else{
        return;
      }
      var flag = false;//Есть ли такая буква в фразе

      for (var i = 0; i < this.currentQuote.length; i++) {
        if(this.currentQuote[i] == letter){
          this.result = this.result.substr(0,i) + this.currentRecord.quote[i] + this.result.substr(i + 1, this.result.length);
          flag = true;
        }
      }

      if(!flag){
        this.mistakes++;
        this.checkMistakes();
      }else{
        if(this.checkSolution()){
          this.mistakes = 4;
          this.gameFlag = false;
        }
      }
    };


    this.isClicked = function(letter){      
      return this.clickedLetters.indexOf(letter) >= 0;
    };

    this.checkMistakes = function(){
      if(this.mistakes >= 3){
        this.gameFlag = false;       
      }
    }

    this.checkSolution = function(){
        for (var i = 0; i < this.result.length; i++) {
          if(this.result[i] == '-') return false;
        }
        return true;
    }

  });

  var records = [
    { quote: 'Жестокость — это порождение злого ума и часто трусливого сердца.' , author: 'Ариосто, Лудовико' },
    { quote: 'Любить иных — тяжёлый крест' , author: 'Пастернак' },
    { quote: 'Будущее, как известно бросает свою тень задолго до того, как войти.' , author: 'Ахматова' },
    { quote: 'Самый трудный поединок — это когда за счастье приходится бороться с ленью.' , author: 'Мохаммед Али' },
    { quote: 'Люди всегда разрушают то, что любят сильнее всего.' , author: 'Оскар Уайльд' },
    { quote: 'Нужно идти туда, куда хочется, а не туда, куда якобы надо.' , author: 'Макс Фрай' },
    { quote: 'Никто не хочет любить в нас обыкновенного человека.' , author: 'Антон Чехов' },
    { quote: 'Твоя жизнь всегда движется в сторону твоей самой сильной мысли.' , author: 'Неизвестный' },
    { quote: 'Быть ребенком — значит учиться жить, Взрослым — учиться умирать.' , author: 'Стивен Кинг' },
    { quote: 'Каждый человек считает страдания, выпавшие на его долю, величайшими.' , author: 'Герман Гессе' },
    { quote: 'Если хочешь иметь то, что никогда не имел, придется делать то, что никогда не делал.' , author: 'Коко Шанель' },
    { quote: 'Благодаря твоим сомнениям совершенствуется мир.' , author: 'Ауробиндо' },
  ];

})();
